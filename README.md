0. Installer VirtualBox
1. Telecharger l'ISO TrueNAS_SCALE:
=> https://www.truenas.com/download-truenas-scale/
2. Formater par TrueNAS_SCALE via image ISO
    2.1 Hostname: HomeServer
3. Changer la configuration réseau sur VirtualBox en (Accès par Pont)
4. Add 2*hard Drive
5. configurer RAID1 (Miroring) à partir de ces deux disque ajoutés
6. Créer FS dédier à un user
7. Créer le dossier partager
8. Créer l'user
9. Activer le service de partage
10. installer Portainer:
> docker volume create portainer_data

> docker run -d -p 8000:8000 -p 9443:9443 --name portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ce:latest
11. Changer le repo de template App sur Portainer
=> https://raw.githubusercontent.com/Qballjos/portainer_templates/master/Template/template.json
12. Changer les droits système sur le partage créé via TrueNAS et les données à www-data:www-data comme user owner et group.
13. installer NextCloud via trusNAS
14. Voir les autre applications d'edition qu'on peux installer sur NextCloud et utiliser(ex: Office / next office)
15. Connexion à Internet:
Dynamic DNS avec DuckDNS |OR| avec : remote.it

16. Comunication server (ex Whatsapp/Telegram/Signal)
16.1 Documentations:
 https://computingforgeeks.com/run-synapse-matrix-homeserver-in-docker-containers/
https://matrix.org/docs/legacy/understanding-synapse-hosting/